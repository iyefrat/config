;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; refresh' after modifying this file!


;; These are used for a number of things, particularly for GPG configuration,
;; some email clients, file templates and snippets.
(setq user-full-name "Itai Y. Efrat"
      user-mail-address "itai3397@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
(setq doom-font (font-spec :family "Inconsolata Nerd Font Mono" :size 40))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. These are the defaults.
;;(setq doom-theme 'doom-city-lights)

(add-hook! 'doom-load-theme-hook
  (custom-set-faces!
    `(show-paren-match :foreground nil :background ,(doom-color 'selection))))

(use-package doom-themes
 :config
 (load-theme 'doom-city-lights t)

  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)

  ;; Enable custom neotree theme (all-the-icons must be installed!)
  (doom-themes-neotree-config)
  ;; or for treemacs users
  (setq doom-themes-treemacs-theme "doom-colors") ; use the colorful treemacs theme
  (doom-themes-treemacs-config)

  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config))

;; If you intend to use org, it is recommended you change this!
(setq org-directory "~/org/")

;; If you want to change the style of line numbers, change this to `relative' or
;; `nil' to disable it:
(setq display-line-numbers-type 'relative)


;; lisp the rainbow
(setq rainbow-delimiters-max-face-count    9)
(setq show-paren-delay 0.05)

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', where Emacs
;;   looks when you load packages with `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c g k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c g d') to jump to their definition and see how
;; they are implemented.

;; my stuff


(setq evil-tex-user-env-map-generator-alist
      '(("f" "\\begin{figure}[!ht]" . "\\end{figure}")))

(setq evil-tex-user-cdlatex-accents-map-generator-alist
      '(("b" . "mathbb")))

(setq reftex-default-bibliography "$HOME/Dropbox/Casimir/bibliography/bib.bib")
(add-hook 'bookmark-after-jump-hook #'+workspaces-add-current-buffer-h)
(add-hook 'magit-mode-hook #'magit-todos-mode)



;;(setq org-superstar-headline-bullets-list '("◉" "○" "✸" "✿" "❀" "◖" "◇" "✚" "✜" "◆" "☢" "◆"))
;;(setq org-superstar-headline-bullets-list '("☰" "☱" "☲" "☳" "☴" "☵" "☶" "☷" "☷" "☷" "☷"))
(setq org-superstar-headline-bullets-list '("㊐" "㊊" "㊋" "㊌" "㊍" "㊎" "㊏"))
;;(setq org-superstar-headline-bullets-list '("㊀" "㊁" "㊂" "㊃" "㊄" "㊅" "㊆" "㊇" "㊈" "㊉"))



(setq +latex-viewers '(zathura))
(after! tex
  (map!
   :map LaTeX-mode-map
   :localleader
   :desc "View" "v" #'TeX-view
   :desc "Toggle TeX-Fold" "f" #'TeX-fold-mode
   :desc "Compile" "c" (cmd! () (TeX-command "LatexMk" 'TeX-master-file))))
(setq TeX-electric-sub-and-superscript nil) ;; don't auto insert braces on ^-
;;(setq font-latex-fontify-script nil)
(add-hook 'LaTeX-mode-hook #'evil-tex-mode)

(add-hook! 'rainbow-mode-hook
  (hl-line-mode (if rainbow-mode -1 +1)))

(setq which-key-idle-delay 0.3)

(use-package! sxhkd-mode
	      :mode "sxhkdrc\\'")

(defun what-face (pos)
  (interactive "d")
  (let ((face (or (get-char-property pos 'read-face-name)
                  (get-char-property pos 'face))))
    (if face (message "Face: %s" face) (message "No face at %d" pos))))

(map!
 :leader
 :prefix ("d" . "modes")
 :desc "rss" "r" #'elfeed
 :desc "torrent" "t" #'transmission)


(setq elfeed-feeds
      '("https://lexi-lambda.github.io/feeds/all.rss.xml"
        "https://xkcd.com/rss.xml"
        "https://steveklabnik.com/atom.rss"))

;(use-package dante
;  :config
; (when (featurep! :checkers syntax)
;    (flycheck-add-next-checker 'haskell-dante '(info . haskell-hlint))))

;(use-package lsp-haskell
; :ensure t
; :config
; (setq lsp-haskell-process-path-hie "haskell-language-server-wrapper")
; ;; Comment/uncomment this line to see interactions between lsp client/server.
; (setq lsp-log-io t)
;)
;; for native-comp libvterm
(setq vterm-module-cmake-args "-USE_SYSTEM_LIBVTERM=yes")

(defadvice! +vterm--dont-resolve-to-eln-file-a (orig-fn &rest args)
  :around #'vterm-module-compile
  (let ((load-suffixes (remove ".eln" load-suffixes)))
    (apply orig-fn args)))

;; when making this literate,
;; use this: rmh-elfeed-org-tree-id to select the elfeed org subtree
(setq rmh-elfeed-org-files  nil );(list "~/org/elfeed.org"))

;(use-package! org-super-agenda
;  :commands (org-super-agenda-mode))
;(after! org-agenda
;  (org-super-agenda-mode))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(safe-local-variable-values
   '((eval when
           (and
            (buffer-file-name)
            (not
             (file-directory-p
              (buffer-file-name)))
            (string-match-p "^[^.]"
                            (buffer-file-name)))
           (unless
               (featurep 'package-build)
             (let
                 ((load-path
                   (cons "../package-build" load-path)))
               (require 'package-build)))
           (unless
               (derived-mode-p 'emacs-lisp-mode)
             (emacs-lisp-mode))
           (package-build-minor-mode)
           (setq-local flycheck-checkers nil)
           (set
            (make-local-variable 'package-build-working-dir)
            (expand-file-name "../working/"))
           (set
            (make-local-variable 'package-build-archive-dir)
            (expand-file-name "../packages/"))
           (set
            (make-local-variable 'package-build-recipes-dir)
            default-directory)))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(show-paren-match ((t (:foreground nil :background "#718CA1")))))
