export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_DIRS="$HOME/.nix-profile/share:/usr/local/share:/usr/share"
#export QT_SCALE_FACTOR=2
export XDG_MUSIC_DIR="$HOME/music"
source .nix-profile/etc/profile.d/nix.sh


export PATH="$HOME/.poetry/bin:$PATH"
export NIX_PATH=$HOME/.nix-defexpr/channels${NIX_PATH:+:}$NIX_PATH
