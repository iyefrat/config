"Plugin time

call plug#begin('~/.config/nvim/plugged')


Plug 'vim-airline/vim-airline'

"Plug 'machakann/vim-sandwich'

Plug 'tpope/vim-surround'

Plug 'tpope/vim-fugitive'

Plug 'Rigellute/rigel'

Plug 'neovimhaskell/haskell-vim'

Plug 'sirver/ultisnips'
    let g:UltiSnipsExpandTrigger = '<tab>'
    let g:UltiSnipsJumpForwardTrigger = '<tab>'
    let g:UltiSnipsJumpBackwardTrigger = '<s-tab>'
    let g:UltiSnipsSnippetDirectories=['/home/itai/.config/nvim/snips']

Plug 'lervag/vimtex'
call plug#end()

"colors
set termguicolors
syntax enable
colorscheme rigel

"set local leader

:let maplocalleader = " "

"line number

set number relativenumber

"latex stuff

let g:tex_flavor = 'latex'
let g:vimtex_latexmk_progname= '/usr/bin/nvr'
let g:vimtex_view_method = 'zathura'
let g:vimtex_view_general_viewer = 'zathura'
let g:vimtex_fold_enabled = 1
let g:vimtex_fold_manual = 1

"sandwhich stuff

"let g:sandwich#recipies = deepcopy(g:sandwich#default_recipies)

"airline stuff

let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1

"easy init.vim editing

nnoremap <LocalLeader>ve :e $XDG_CONFIG_HOME/nvim/init.vim<CR>
nnoremap <LocalLeader>vr :source $XDG_CONFIG_HOME/nvim/init.vim<CR>

"Spelling is fun!
set spelllang=en
set spellfile=$HOME/.config/nvim/spell/en.utf-8.add

"fold color stuff
highlight Folded ctermbg=black ctermfg=green

"haskell stuff
syntax on
filetype plugin indent on
